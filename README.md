# 3Delight NSI Language Support

<span style="display:block;text-align:left">![3Delight Logo](https://gitlab.com/gducatel/nsi_vscode_extension/-/raw/master/images/3Delight-Logo-Small.png)![3Delight NSI Logo](https://gitlab.com/gducatel/nsi_vscode_extension/-/raw/master/images/3Delight-NSI-Small.png)</span>
<b>Illumination Research</b> | 3Delight NSI Language Extension for Visual Studio Code.

___
## Features
- 3Delight NSI ASCII syntax highlighting for VSCode.
- All standard NSI keywords.
<span style="display:block;text-align:center">![Example](https://gitlab.com/gducatel/nsi_vscode_extension/-/raw/master/images/example.png)</span>
- Convert NSI file format from ASCII to Binary and Binary to ASCII. Using the following command: <b>3Delight - Convert NSI format</b>
<span style="display:block;text-align:center">![Convert format](https://gitlab.com/gducatel/nsi_vscode_extension/-/raw/master/images/convert-file.gif)</span>
- Start a render to 3Delight display from an open NSI file. Using the following command: <b>3Delight - Render NSI File</b>
<span style="display:block;text-align:center">![Rendering](https://gitlab.com/gducatel/nsi_vscode_extension/-/raw/master/images/rendering.gif)</span>
___
## For more information
- [3Delight](https://www.3delight.com/)
- [3Delight NSI](https://www.3delight.com/documentation/display/3DSP/Rendering+NSI+file)
- [3Delight GitLab](https://gitlab.com/3Delight)