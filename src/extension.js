'use strict';

const vscode = require("vscode");
const os = require('os');
const fs = require('fs');
const util = require('util');
const path = require('path');
const crypto = require("crypto");
const exec = util.promisify(require('child_process').exec);
const open = util.promisify(fs.open);
const read = util.promisify(fs.read);
const close = util.promisify(fs.close);

const maxNsiFileSize = 1024 * 1024 * 500; // 500 MB


/**
 * @param {vscode.ExtensionContext} context
 */

async function isAscii(path) {

	// Try to detect if a file is Ascii or Binary
	var isAsciiBool = false;
	try {
	  const fd = await open(path, 'r');
	  var buffer = Buffer.alloc(8);
	  await read(fd, buffer, 0, 8, 0);
	  isAsciiBool = /^[\x00-\x7F]*$/.test(buffer.toString());
	  await close(fd);
	  return isAsciiBool;
	}
	catch (err) {
		vscode.window.showErrorMessage(err.toString());
		return;
	}
}

function isNsiFile(path) {

	// Looking at the file extension.
	var pathLower = path.toLowerCase();
	var pathLowerExt = pathLower.split('.').pop();
	if(pathLowerExt == 'nsi'){
		return true;
	}
	return false;
}

function checkPermission(path){
	try {
		fs.accessSync(path, fs.constants.W_OK);
		return true;
	}
	catch (err) {
		return false;
	}
}

function checkTokenization(){


	const maxTokenizationLines = vscode.workspace.getConfiguration().get('editor.maxTokenizationLineLength');
	const largeFileOptimizations = vscode.workspace.getConfiguration().get('editor.largeFileOptimizations');

	let editor = vscode.window.activeTextEditor;
	if (!editor) {
		vscode.window.showInformationMessage('No open file!');
		return;
	}
	const numberOfLines = editor.document.lineCount;

	if(numberOfLines > maxTokenizationLines){
		vscode.window.showWarningMessage('VSCode  syntax highlighting deactivated due to the number of lines inside your document (' + numberOfLines.toString() +
			' lines). Change this value inside your preferences (currently at ' + maxTokenizationLines.toString() + ') \"editor.maxTokenizationLineLength\"');
	}
	if(largeFileOptimizations){
		vscode.window.showWarningMessage('VSCode syntax highlighting for large files deactivated. Change this value inside your preferences (currently at ' +
			largeFileOptimizations.toString() + ') \"editor.largeFileOptimizations\"');
	}
	return;
}

async function catNsiFile(inputPath, outputPath) {

	// Convert file from one format to another one
	if(isNsiFile(inputPath)){
		const renderdl = vscode.workspace.getConfiguration().get('nsi.renderdl');
		var command = '';
		var binaryConvertionProcess = false;
		if(await isAscii(inputPath)){
			outputPath += '_binary.nsi'
			command = renderdl + ' -cat -binary -o \"' + outputPath + '\" \"' + inputPath + '\"'
			binaryConvertionProcess = true;
		}
		else{
			outputPath += '_ascii.nsi'
			command = renderdl + ' -cat -o \"' + outputPath + '\" \"' + inputPath + '\"'
		}
		try{
			const { stdout, stderr } = await exec(command, { maxBuffer: maxNsiFileSize });
			fs.renameSync(outputPath, inputPath);
		}
		catch (err){
			try {
				if (fs.existsSync(outputPath)) {
					try {
						fs.unlinkSync(outputPath);
					}catch(err) {}
				}
			} catch(err) {}
			vscode.window.showErrorMessage(err.toString());
			return;
		}

		// Open the new file into vscode when possible
		vscode.commands.executeCommand('workbench.action.closeActiveEditor')
		var openPath = vscode.Uri.parse("file:///" + inputPath);
		vscode.workspace.openTextDocument(openPath).then(doc => {vscode.window.showTextDocument(doc, { preview: false });});

		if(binaryConvertionProcess){

			// Display a message to let the user know to manualy reload the file.
			vscode.window.showWarningMessage('ASCII to Binary done, please reload the file if needed.');
			return;
		}
		vscode.window.showInformationMessage('Convertion to ASCII completed. Document reloaded.');
	}
	else{
		vscode.window.showErrorMessage('You need a 3Delight NSI File for this action.');
	}
}

async function renderNsiFile(inputPath) {

	// Render an NSI File
	if(isNsiFile(inputPath)){
		const renderdl = vscode.workspace.getConfiguration().get('nsi.renderdl');
		try{
			exec(renderdl + ' -id \"' + inputPath + '\"');
		}
		catch (err){
			vscode.window.showErrorMessage(err.toString());
			return;
		}
	}
	else{
		vscode.window.showErrorMessage('You need an NSI File.');
	}
}

function getDocumentPath(){

	let editor = vscode.window.activeTextEditor;
	if (!editor) {
		vscode.window.showErrorMessage('No open file!');
		return false;
	}
	return editor.document.fileName.toString();
}

function activate(context) {
	let nsiConvert = vscode.commands.registerCommand('nsi.convertNsiFileFormat', function () {
		var fullPath = getDocumentPath();
		if(fullPath){
			const id = crypto.randomBytes(16).toString("hex");
			checkTokenization();
			if(checkPermission(fullPath)){
				catNsiFile(fullPath, path.join(os.tmpdir(), id));
			}else{
				vscode.window.showErrorMessage('We cannot convert this NSI Files as we don\'t have the write Permission.');
			}
		}
	});

	let nsiRender = vscode.commands.registerCommand('nsi.renderNsiFile', function () {
		var fullPath = getDocumentPath()
		if(fullPath){
			renderNsiFile(fullPath);
		}
	});

	context.subscriptions.push(nsiConvert);
	context.subscriptions.push(nsiRender);
}
exports.activate = activate;

function deactivate() {}

module.exports = {
	activate,
	deactivate
}
