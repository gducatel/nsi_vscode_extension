# Change Log
All notable changes to the extension will be documented in this file.

## [0.0.5]
- Adding support for .nsi.static files.
- Adding validation that a file is open before executing any command.
- Fix a bug when validating that the 'Optimization for large files' is enabled, misleading users.
- Changing the option that points to the valid renderdl executable.

## [0.0.4]
- Display messages to check if the syntax highlighting will be activated on large files.

## [0.0.3]
- Fix some typos and rename the '3Delight - Convert NSI format' to '3Delight - Convert NSI Format'.

## [0.0.2]
- Generating temp files properly and cleaning them after execution.
- Adding more validations when manipulation files (including error messages).
- Adding default keyboard mapping for the commands.

## [0.0.1]
- Initial release.
- Simple syntax highlighting.
- Convert an NSI Binary file format to an ASCII file format.
- Start a 3Delight render into 3Delight Display directly from an opened NSI file.